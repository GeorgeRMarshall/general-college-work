--Define a Haskell function tear, including its type, which takes a
-- predicate and a list and returns a list of two lists, the first
-- those elements of the input list which pass the predicate, the
-- second those that don't, in order.
-- Examples:
-- tear (>5) [1,10,2,12,3,13] => [[10,12,13],[1,2,3]]

tear :: (a -> Bool) -> [a] -> [[a]]
tear op lst = [[x | x <- lst, op x], [x | x <- lst, not (op x)]]






