--2019 CS424 January Exam Question
--Define scatterGather (see Q1) in Haskell, except that it takes an
--additional first argument which is the value to use when an index
--it out of range.

scatterGather :: Char -> [Int] -> [Char] -> [Char]
scatterGather a b c = [(replaceChar a x c) | x <- b]

replaceChar :: Char -> Int -> [Char] -> Char
replaceChar z a b
 | (a < length b) = b!!a
 | otherwise = z


