--Define a haskell function before which takes a predicate p and a list xs and returns a list of all 
--elements of xs which occur immediately before an element of which p is true.
import Data.List ( findIndices )

before :: (a -> Bool) -> [a] -> [a] 
before op lst = [ lst !!(c-1) | c <- findIndices op lst]

--line 1 imports findIndices from Data.list to use later. Data list is a very basic libary
--find indices returns a int list of all locations of any entry that matches the inputed operator
--line 2 takes in the operator (op) and then a list [a] it then returns a list.
--line 3 each entry from the findIndices is "mapped" to c, it then adds the entry at the location c-1 to the list and returns the list.
